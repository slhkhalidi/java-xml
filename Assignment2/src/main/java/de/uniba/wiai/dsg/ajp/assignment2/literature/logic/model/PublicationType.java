package de.uniba.wiai.dsg.ajp.assignment2.literature.logic.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum PublicationType {
	@XmlEnumValue("ARTICLE")
	ARTICLE,
	@XmlEnumValue("TECHREPORT")
	TECHREPORT,
	@XmlEnumValue("BOOK")
	BOOK,
	@XmlEnumValue("MASTERSTHESIS")
	MASTERSTHESIS,
	@XmlEnumValue("PHDTHESIS")
	PHDTHESIS,
	@XmlEnumValue("INPROCEEDINGS")
	INPROCEEDINGS

}
