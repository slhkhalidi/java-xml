package de.uniba.wiai.dsg.ajp.assignment2.literature;


import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.LiteratureDatabaseException;
import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.MainService;
import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.impl.MainServiceImpl;
import java.io.IOException;

public class Main {


	public static void main(String[] args)   throws IOException, LiteratureDatabaseException {

		MainService service = new MainServiceImpl();
		((MainServiceImpl) service).startService();

	}
}

