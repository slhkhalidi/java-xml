package de.uniba.wiai.dsg.ajp.assignment2.literature.logic.impl;

import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.DatabaseService;
import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.LiteratureDatabaseException;
import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.MainService;
import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.model.Author;
import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.model.Publication;
import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.model.PublicationType;
import de.uniba.wiai.dsg.ajp.assignment2.literature.ui.ConsoleHelper;

import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.model.Database;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

public class MainServiceImpl implements MainService {

	boolean exit = false;

	/**
	 * Default constructor required for grading.
	 */
	public MainServiceImpl() {
		/*
		 * DO NOT REMOVE - REQUIRED FOR GRADING
		 *
		 * YOU CAN EXTEND IT BELOW THIS COMMENT
		 */
	}

	/**
	 * Validates the XML file identified by <code>path</code> with an XML Schema
	 * file.
	 *
	 * @param path the path to the XML file to be validated
	 * @throws LiteratureDatabaseException if the file identified by
	 *                                     <code>path</code> is not valid
	 *                                     and when path is not a regular file or does not exist
	 */
	@Override
	public  void validate(String path) throws LiteratureDatabaseException {
		try {
			Path file = Paths.get(path);
			if(!Files.isRegularFile(file)){
				throw new LiteratureDatabaseException("The file is not a regular file.");
			}
			if(Files.notExists(file)){
				throw new LiteratureDatabaseException("The file does not exist.");
			}
			SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = sf.newSchema(new File("schema_file.xsd"));

			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(new File("database.xml")));
		}
		catch (SAXException | IOException e) {
			e.printStackTrace();
			System.out.println("Error!");
		}
		System.out.println("Validation successful!");
	}

	/**
	 * Loads a XML file by unmarshalling it into memory.
	 *
	 * @param path the path of the XML file to be unmarshalled
	 * @return a service handle (<code>DatabasService</code>) for manipulating the
	 *         literature database
	 * @throws LiteratureDatabaseException when path is not a regular file or does not exist
	 */
	@Override
	public DatabaseService load(String path) throws LiteratureDatabaseException {
		DatabaseService newCreatedDatabaseService = null;
		try{
			JAXBContext context = JAXBContext.newInstance(Database.class);
			//Create Unmarshaller object for different xml file
			Unmarshaller um = context.createUnmarshaller();

			//Unmarshal from xml file to database
			Database newdatabase = (Database) um.unmarshal(new File(path));
			newCreatedDatabaseService = new DatabaseServiceImpl(newdatabase);
		}catch (JAXBException e){
			e.printStackTrace();
		}
		return newCreatedDatabaseService;
	}

	/**
	 * Creates a new and empty literature database.
	 *
	 * @return a service handle (<code>DatabaseService</code>) for manipulating the
	 *         literature database
	 * @throws LiteratureDatabaseException
	 */
	@Override
	public DatabaseService create() throws LiteratureDatabaseException {
		Database emptyDatabase = new Database();
		return new DatabaseServiceImpl(emptyDatabase);
	}

	/**
	 * Starts the user interface
	 *
	 * @throws IOException
	 * @throws LiteratureDatabaseException
	 */
	public void startService() throws IOException, LiteratureDatabaseException {
		ConsoleHelper console = ConsoleHelper.build();
		mainMenu(console);
	}

	/**
	 * Builds the main menu and acts according to input
	 *
	 * @param console is reading the inputs by the user
	 * @throws IOException if an error occurs during reading from or writing to a stream
	 * @throws LiteratureDatabaseException if an invalid database access occurs
	 */
	private void mainMenu(ConsoleHelper console) throws IOException, LiteratureDatabaseException {
		while(!exit){
			printMainMenu();
			// reads the user input to react accordingly
			int number = console.askIntegerInRange("Please enter a number:",0,2);
			switch(number) {
				case 0:
					exit = true;
					break;
				case 1:
					String DBpath = console.askNonEmptyString("Please enter the database path:");
					validate(DBpath);
					DatabaseService database = load(DBpath);
					subMenu(console, database);
					break;
				case 2:
					DatabaseService newDB = create();
					subMenu(console, newDB);
					break;
			}
		}
	}

	/**
	 * Builds sub menu and acts according to input
	 *
	 * @param console is reading the inputs by the user
	 * @param database is the given database from main menu
	 * @throws IOException if an error occurs during reading from or writing to a stream
	 * @throws LiteratureDatabaseException if any of the used functions access an invalid database
	 */
	private void subMenu(ConsoleHelper console, DatabaseService database) throws IOException, LiteratureDatabaseException {
		boolean exitSub = false;
		while(!exitSub) {
			printSubMenu();
			// reads the user input to react accordingly
			int number = console.askIntegerInRange("Please enter a number:",0,8);
			switch(number){
				case 0:
					exitSub=true;
					break;
				case 1:
					String name = console.askString("Please enter the author's name:");
					String email = console.askString("Please enter the author's email address:");
					String AutID = console.askString("Please enter the author's ID:");
					database.addAuthor(name,email,AutID);
					break;
				case 2:
					removeAuthor(console,database);
					break;
				case 3:
					String title = console.askString("Please enter the title:");
					int yearPublished = console.askInteger("Please enter the publishing-year:");
					PublicationType type = getType(console);
					List<String> authorIDs = createIDList(console);
					String PubID = console.askString("Please enter the publication ID:");
					database.addPublication(title,yearPublished,type,authorIDs,PubID);
					break;
				case 4:
					removePublication(console, database);
					break;
				case 5:
					List<Author> authors = database.getAuthors();
					String autResult = authors.toString();
					System.out.println(autResult);
					break;
				case 6:
					List<Publication> pubs = database.getPublications();
					String pubResult = pubs.toString();
					System.out.println(pubResult);
					break;
				case 7:
					database.printXMLToConsole();
					break;
				case 8:
					String path = console.askString("Please enter the path the XML file should be saved to:");
					database.saveXMLToFile(path);
					break;
			}
		}
	}

	/**
	 * Asks for input until the User gives a valid type
	 *
	 * @param console is reading the inputs by the user
	 * @return the publication type entered by the user
	 * @throws IOException
	 */
	private PublicationType getType(ConsoleHelper console) throws IOException {
		PublicationType result = null;
		boolean leaveWhile = false;
		List<String> types = new LinkedList<String>();
		types.add("ARTICLE");
		types.add("TECHREPORT");
		types.add("BOOK");
		types.add("MASTERSTHESIS");
		types.add("PHDTHESIS");
		types.add("INPROCEEDINGS");
		while(!leaveWhile) {
			String input = console.askString("Please enter one of the following publication types (in capital letters!)\n" +
					"ARTICLE, TECHREPORT, BOOK, MASTERSTHESIS, PHDTHESIS, INPROCEEDINGS:");
			if(!types.contains(input)) {
				System.out.println("Invalid publication type, please try again!");
				continue;
			}else {
				result = PublicationType.valueOf(input);
				leaveWhile = true;
			}
		}
		return result;
	}

	/**
	 * Creates the list of author IDs involved in the publication
	 *
	 * @param console is reading the inputs by the user
	 * @return the resulting list of author IDs
	 * @throws IOException if an error occurs during reading from or writing to a stream
	 */
	private List<String> createIDList(ConsoleHelper console) throws IOException {
		List<String> idList = new LinkedList<String>();
		boolean leaveWhile = false;
		while(!leaveWhile) {
			System.out.println(    	"( 1 ) Add ID to author list\n" +
									"( 2 ) Author list done");
			int number = console.askIntegerInRange("Please enter a number:",1,2);
			switch(number){
				case 1:
					String newID = console.askString("Please enter the author's ID:");
					idList.add(newID);
					break;
				case 2:
					leaveWhile = true;
					break;
			}
		}
		return idList;
	}

	/**
	 * Prints the main menu for the user interface
	 */
	public void printMainMenu() {
		String menu =  	"( 1 ) Load and Validate Literature Database\n" +
						"( 2 ) Create New Literature Database\n" +
						"( 0 ) Exit System";
		System.out.println(menu);
	}

	/**
	 * Prints the sub menu for the user interface
	 */
	public void printSubMenu() {
		String menu =  	"( 1 ) Add Author\n" +
						"( 2 ) Remove Author\n" +
						"( 3 ) Add Publication\n" +
						"( 4 ) Remove Publication\n" +
						"( 5 ) List Authors\n" +
						"( 6 ) List Publications\n" +
						"( 7 ) Print XML on Console\n" +
						"( 8 ) Save XML to File\n" +
						"( 0 ) Back to main menu / close without saving";
		System.out.println(menu);
	}

	/**
	 * Differs between removing all authors or just one
	 *
	 * @param console is reading the inputs by the user
	 * @param database is the given database
	 * @throws IOException if an error occurs during reading from or writing to a stream
	 * @throws LiteratureDatabaseException if any of the used functions access an invalid database
	 */

	private void removeAuthor(ConsoleHelper console, DatabaseService database) throws IOException, LiteratureDatabaseException {
		System.out.println(   	"( 1 ) Remove all authors\n" +
								"( 2 ) Remove one author");
		int number = console.askIntegerInRange("Please enter a number:",1,2);
		switch(number){
			case 1:
				database.deleteAllAuthors();
				break;
			case 2:
				String rmAutID = console.askString("Please enter the ID of the author to be removed:");
				database.removeAuthorByID(rmAutID);
				break;
		}
	}
	
	/**
	 * Differs between removing all publications or just one
	 *
	 * @param console is reading the inputs by the user
	 *
	 * @param database is the given database
	 *
	 * @throws IOException if an error occurs during reading from or writing to a stream
	 *
	 * @throws LiteratureDatabaseException if any of the used functions access an invalid database
	 */
	private void removePublication(ConsoleHelper console, DatabaseService database) throws IOException, LiteratureDatabaseException {
		System.out.println(     "( 1 ) Remove all publications\n" +
								"( 2 ) Remove one publication");
		int number = console.askIntegerInRange("Please enter a number:",1,2);
		switch(number){
			case 1:
				database.deleteAllPublication();
				break;
			case 2:
				String rmPubID = console.askString("Please enter the ID of the publication to be removed:");
				database.removePublicationByID(rmPubID);
				break;
		}
	}
}


			
