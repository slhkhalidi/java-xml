package de.uniba.wiai.dsg.ajp.assignment2.literature.logic.impl;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;


import java.util.LinkedList;
import java.util.List;
import java.util.Set;


import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.DatabaseService;
import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.LiteratureDatabaseException;
import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.ValidationHelper;


import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.model.Author;
import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.model.Database;
import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.model.Publication;
import de.uniba.wiai.dsg.ajp.assignment2.literature.logic.model.PublicationType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class DatabaseServiceImpl implements DatabaseService {


	public final Database database = new Database();

	//constructor for create and load method in MainServiceImpl
    public DatabaseServiceImpl(Database emptyDatabase) { }


    @Override
	public void addPublication(String title, int yearPublished, PublicationType type, List<String> authorIDs, String id)
			throws LiteratureDatabaseException {
		if (title == null || title.isEmpty()) {
			throw new LiteratureDatabaseException("Error in Title");
		}
		if (yearPublished < 0) {
			throw new LiteratureDatabaseException("Error in  Year");
		}
		if (type==null) {
			throw new LiteratureDatabaseException("Error in Type");
		}
		if (authorIDs==null || authorIDs.isEmpty() || duplicateId(authorIDs))  {
			throw new LiteratureDatabaseException("Error in Author ID");
		}
		if (id ==null || id.isEmpty() || !ValidationHelper.isId(id)) {
			throw new LiteratureDatabaseException("Error in Publication ID");
		}
		else {
			Publication publication = new Publication();
			publication.setTitle(title);
			publication.setYearPublished(yearPublished);
			publication.setType(type);
			publication.setId(id);
			List<Author> authors = new LinkedList<>();
			for (int i = 0; i < database.getAuthors().size(); i++) {
				if (authorIDs.contains(database.getAuthors().get(i).getId())) {
					authors.add(database.getAuthors().get(i));
				}
			}
			publication.setAuthors(authors);

			database.getPublications().add(publication);
		}
	}

	@Override
	public void removePublicationByID(String id) throws LiteratureDatabaseException {
		if (!ValidationHelper.isId(id) || !publicationIdExist(id) ) {
			throw new LiteratureDatabaseException("Error: Publication ID is invalid");
		}
		else {
			deletePublication(id);
		}
	}

	@Override
	public void removeAuthorByID(String id) throws LiteratureDatabaseException {
	if(!ValidationHelper.isId(id) || !authorIdExist(id)) {
			throw new LiteratureDatabaseException("Error: Author ID is invalid");
		}
		else {
			deleteAuthor(id);
		}
	}

	@Override
	public void addAuthor(String name, String email, String id) throws LiteratureDatabaseException {
		if(name==null || name.isEmpty()){
			throw new LiteratureDatabaseException("Error in Name");
		}
		if(!ValidationHelper.isEmail(email) || email.isEmpty()){
			throw new LiteratureDatabaseException("Error in Email");
		}
		if(!ValidationHelper.isId(id) || id.isEmpty()){
			throw new LiteratureDatabaseException("Error in Id");
		}
		Author author = new Author();
		author.setName(name);
		author.setEmail(email);
		author.setId(id);
		database.getAuthors().add(author);
	}


	@Override
	public List<Publication> getPublications() {
		return database.getPublications();
	}

	@Override
	public List<Author> getAuthors() {
		return database.getAuthors();
	}


	@Override
	public void clear() {
	       deleteAllAuthors();
		   deleteAllPublication();
	}




	@Override
	public void printXMLToConsole() throws LiteratureDatabaseException {
		try {
			JAXBContext context = JAXBContext.newInstance(Database.class);

			Unmarshaller us = context.createUnmarshaller();
			Database database = (Database) us.unmarshal(new File("database.xml"));

			Marshaller ms = context.createMarshaller();
			ms.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			ms.marshal(database, System.out);

		} catch (JAXBException e){
			e.printStackTrace();
		}
	}

	@Override
	public void saveXMLToFile(String path) throws LiteratureDatabaseException{
		try {
			Path path1 = Paths.get(path);
			if (Files.notExists(path1)) {
				throw new IOException("Path does not exists");
			}
			JAXBContext context = JAXBContext.newInstance(Database.class);


			Marshaller ms = context.createMarshaller();
			ms.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			ms.marshal(database, new File("DATABASE.xml"));
		}catch (JAXBException | IOException e){
			e.printStackTrace();
		}
	}

////////////////////////////////////////////////  NEW METHODS   /////////////////////////////////////////////////////


	public boolean duplicateId(List<String> authorsIds){
		boolean result=true;
		//create a new empty HashSet List //
		//HashSet removes duplicates//
		Set<String> newlist = new HashSet<String>();
		for(String id : authorsIds) {
			if (!newlist.contains(id)) {
				newlist.add(id);
				result=false;
			}
			else {
				result=true;
				break;
			}
		}
		return result;
	}

	//Check are the PublicationId  Exists or not? //
	public boolean publicationIdExist(String id) {
			List<Publication> publications = database.getPublications();
		for (Publication publication : publications) {
			if (publication.getId().equals(id))
				return true;
		}
		return false;
	}


	//Check are the AuthorId  Exists or not? //
	public boolean authorIdExist(String id) {
		List<Author> authors = database.getAuthors();
			for (Author author : authors) {
				if (author.getId().equals(id))
					return true;
			}
			return false;
		}




	// Delete Publication with ID//
	public void deletePublication (String id) {
		for(int i=0; i< database.getPublications().size(); i++){
			if(database.getPublications().get(i).getId().equals(id)) {
				database.getPublications().remove(i);
			}
		} }


	// Delete Author with ID //
	public void deleteAuthor(String id) {
		for(int i=0; i<database.getAuthors().size(); i++){
			if(database.getAuthors().get(i).getId().equals(id)){
				database.getAuthors().remove(i);
			}
		}
	}


	//Delete all Publications//
	public void deleteAllPublication() {
		if(database.getPublications().isEmpty()) {
			System.out.println("No publications exist in this database!");
		}
		for (int i = 0; i < database.getPublications().size(); i++) {
			database.getPublications().remove(i);
		}
	}


	//Delete all Authors//
	public void deleteAllAuthors() {
		if(database.getAuthors().isEmpty()) {
			System.out.println("No authors exist in this database!");
		}
		for(int i=0; i<database.getAuthors().size(); i++){
			database.getAuthors().remove(i);
		}
	}



/////////////////////////////////////////          END NEW METHODS       ///////////////////////////////////////

}




